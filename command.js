let paragraphs = document.querySelectorAll("p");
paragraphs.forEach(changeColor);
function changeColor(elem) {
  elem.style.backgroundColor = "#ff0000";
}

// 

let element = document.getElementById("optionsList");
console.log(`Елемент із id="optionsList" ${element}. А саме:`);
console.log(element);
console.log(`Його батьківський елемент: ${element.parentElement}. А саме:`);
console.log(element.parentElement);

element = document.getElementById("optionsList");
element.hasChildNodes(); // true

console.log('Дочірні ноди елемента із id="optionsList":');
element = document.getElementById("optionsList").childNodes;
for (let i of element) {
  console.log("Нода: " + i + ", " + "тип ноди: " + typeof i);
}

// 

let contentTestParagraph = document.getElementById("testParagraph");
contentTestParagraph.textContent = "This is a paragraph";

// 

let elementsMainHeader = document.querySelector(".main-header").children;
console.log('\n Елементи, вкладені в елемент із класом "main-header":');
console.log(elementsMainHeader);

for (let element of elementsMainHeader) {
  element.classList.add("nav-item");
}

// 

let elementsSectionTitle = document.querySelectorAll(".section-title");
for (let element of elementsSectionTitle) {
  element.classList.remove("section-title");
}
